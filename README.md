## Micronaut LinkedIn AccessToken

Obtén tu userId y genera un AccessToken para usar en la red de Linkedin

## Build

- clona este repositorio
 
- crea un fichero `.env`
 
```env
LINKEDIN_CLIENT_ID=xxxxxx
LINKEDIN_SECRET=yyyy
```

- ejecuta la aplicación

`./gradlew run`

- abre un navegador en http://localhost:8080 y autentificate