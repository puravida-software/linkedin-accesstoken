package com.puravida.model;

import io.micronaut.core.annotation.Introspected;

/**
 * @author : jorge <jorge.aguilera@seqera.io>
 */
@Introspected
public class ProfilePicture {

    final String displayImage;

    public ProfilePicture(String displayImage) {
        this.displayImage = displayImage;
    }

    public String getDisplayImage() {
        return displayImage;
    }
}
