package com.puravida.model;

import io.micronaut.core.annotation.Introspected;
import io.micronaut.core.annotation.NonNull;
import javax.validation.constraints.NotBlank;

/**
 * @author : jorge <jorge.aguilera@seqera.io>
 */
@Introspected
public class LinkedInMe {
    @NonNull
    @NotBlank
    private final String id;

    @NonNull
    @NotBlank
    private final String localizedFirstName;

    @NonNull
    @NotBlank
    private final String localizedLastName;

    private final ProfilePicture profilePicture;

    public LinkedInMe(@NonNull String id,
                      @NonNull String localizedFirstName,
                      @NonNull String localizedLastName,
                      ProfilePicture profilePicture) {
        this.id = id;
        this.localizedFirstName = localizedFirstName;
        this.localizedLastName = localizedLastName;
        this.profilePicture = profilePicture;
    }

    @NonNull
    public String getId() {
        return id;
    }

    @NonNull
    public String getLocalizedFirstName() {
        return localizedFirstName;
    }

    @NonNull
    public String getLocalizedLastName() {
        return localizedLastName;
    }

    @NonNull
    public ProfilePicture getProfilePicture() {
        return profilePicture;
    }

}
