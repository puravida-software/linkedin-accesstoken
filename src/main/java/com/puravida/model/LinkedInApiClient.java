package com.puravida.model;

import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Header;
import io.micronaut.http.annotation.Post;
import io.micronaut.http.client.annotation.Client;
import org.reactivestreams.Publisher;

import java.util.Map;

/**
 * @author : jorge <jorge.aguilera@seqera.io>
 */

@Client(id = "linkedin")
@Header(name = "X-Restli-Protocol-Version", value = "2.0.0")
@Header(name="LinkedIn-Version", value="X-Restli-Protocol-Version")
public interface LinkedInApiClient {

    @Get("/v2/me")
    Publisher<LinkedInMe> me(@Header String authorization);

    @Post("/v2/ugcPosts")
    Publisher<Map> post(@Header String authorization, @Body Map post);

}
