package com.puravida.input;


import io.micronaut.context.annotation.Value;
import io.micronaut.core.annotation.Nullable;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Produces;
import io.micronaut.security.annotation.Secured;
import io.micronaut.security.authentication.Authentication;
import io.micronaut.security.rules.SecurityRule;
import io.micronaut.views.View;
import io.micronaut.views.turbo.TurboFrameView;

import java.util.Map;

/**
 * @author : jorge <jorge.aguilera@seqera.io>
 *
 */
@Controller("/")
class IndexController {

    IndexController() {
    }

    @Get("/")
    @Secured(SecurityRule.IS_ANONYMOUS)
    @View("index")
    Map index(HttpRequest<?> request, @Nullable Authentication auth) {
        return Map.of();
    }

}
